<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db' => array(
        'driver'   => 'Pdo',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
        'username' => 'zfs-demo',
        'password' => 'zfs-demo',
        'dsn'      => 'mysql:dbname=zfs-demo;host=localhost',
    ),
    'mail' => array(
        'transport' => array(
            'host' => '10.10.0.114',
            'name' => '10.10.0.114',
            'port' => 2525
        ),
        'defaultFrom' => array(
            'email' => 'zfstarter@nixsolutions.com',
            'name' => 'ZFStarter Demo'
        ),
        'headers' => array(
            'PROJECT' => 'zfstarter',
            'EXTERNAL' => 'true'
        ),
    ),
    'opauth' => array(
        'security_salt' => 'f2a0177fadfdb1a7c841c672144693587e224857d230e7f53db1271b49c0b925',
        'Strategy' => array(
            'Facebook' => array(
                'app_id'     => '1542221412669246',
                'app_secret' => '8a6b764fa3dafd590623f145cff054d3',
                'scope'      => 'email'
            ),
            'Twitter' => array(
                'key'    => 'iJHaUTVDdayVTKoWgm7J1LQPh',
                'secret' => 'mlabUpNO00I6zmyfK43BcKSwzxBpFz8c8LXWEnvx0ZvZyimEWt'
            ),
            'Google' => array(
                'client_id'     => '600496275657-k3dph3jf7gbtojcrt10m4n4nhor7iujc.apps.googleusercontent.com',
                'client_secret' => 'xsrhgInrnY0iStVjXSL95Pta'
            )
        ),
        'callback_transport' => 'post'
    ),
    'ZFCTool' => array(
        'migrations' => array(
            'modulesDirectoryPath' => array(
                'module',
                'vendor/zfstarter'
            ),
        ),
    )
);
