ZFStarter Demo Application
=======================

Installation
------------

Step 1: Clone or download Demo Application from Bitbucket

    cd my/project/dir
    git clone https://farior@bitbucket.org/farior/zfstarter-demo.git
    cd zfstarter-demo

Step 2: Install dependencies

    php composer.phar self-update
    php composer.phar install

Step 3: Set up db config

    create config file in my/project/dir/config/autoload/local.php

    'db' => array(
            'username' => 'root',
            'password' => '',
            'dsn'      => 'mysql:dbname=yourdbname;host=localhost',
        ),

Step 4: Apply migrations

    php vendor/bin/zfc.php up db -i